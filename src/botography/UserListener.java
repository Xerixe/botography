package botography;

import org.pircbotx.hooks.ListenerAdapter;
import org.pircbotx.hooks.events.JoinEvent;
import org.pircbotx.hooks.events.MessageEvent;

/**
 * Listens for user actions (join, messages) and forwards events to
 * PigeonManager.
 * 
 * @author cstohr
 * 
 */
public class UserListener extends ListenerAdapter {
	/**
	 * Channel chat
	 * 
	 * @param event
	 * @throws java.lang.Exception
	 */
	@Override
	public void onMessage(final MessageEvent event) throws Exception {
		PigeonManager.userMessage(event);
	}

	@Override
	public void onJoin(final JoinEvent event) throws Exception {
		PigeonManager.userJoin(event);
	}
}
