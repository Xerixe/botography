package botography;

import java.util.ArrayList;
import java.util.HashMap;

import org.pircbotx.hooks.events.JoinEvent;
import org.pircbotx.hooks.events.MessageEvent;

/**
 * Manages messages to users.
 * 
 * @author cstohr
 * 
 */
public class PigeonManager {

	private static HashMap<String, ArrayList<String>> m_pigeonMap = new HashMap<String, ArrayList<String>>();

	public static void userMessage(final MessageEvent event) {
		String user = event.getUser().getNick();
		ArrayList<String> messages = m_pigeonMap.remove(user);
		if (messages == null) {
			return;
		}
		for (String message : messages) {
			event.respond(message);
		}
	}

	public static void userJoin(final JoinEvent event) {
		String user = event.getUser().getNick();
		ArrayList<String> messages = m_pigeonMap.remove(user);
		if (messages == null) {
			return;
		}
		for (String message : messages) {
			event.respond(message);
		}
	}

	public static boolean newPigeon(final String user, final String message) {
		ArrayList<String> messages = m_pigeonMap.get(user);
		if (messages == null) {
			messages = new ArrayList<String>();
		}
		messages.add(message);
		m_pigeonMap.put(user, messages);
		return true;
	}
}
