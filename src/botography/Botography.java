/*
    This program is a part of Botography which is an IRC bot.

    Copyright (C) 2016 Gerald Lam

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */
package botography;

import java.nio.charset.Charset;

import javax.net.ssl.SSLSocketFactory;

import org.pircbotx.Configuration;
import org.pircbotx.PircBotX;
import org.pircbotx.hooks.managers.ThreadedListenerManager;

import botography.messages.CommandProcessor;

/**
 * 
 * @author Xerixe
 */
public class Botography {

	public static void main(final String[] args) throws Exception {
		Settings.init();
		CommandProcessor.init();
		Configuration configuration = new Configuration.Builder()
				.setName(Settings.getName())
				.setLogin(Settings.getLogin())
				.setRealName(Settings.getName())
				.addAutoJoinChannel(Settings.getChannel())
				.setListenerManager(new ThreadedListenerManager())
				.addListener(new CommandListener())
				.addListener(new UserListener())
				.addServer(Settings.getServer(), Settings.getServerPort())
				// .addCapHandler(new SASLCapHandler(Settings.getLogin(),
				// Settings.getPassword()))
				.setSocketFactory(SSLSocketFactory.getDefault())
				.setEncoding(Charset.forName("UTF-8")).buildConfiguration();
		PircBotX bot = new PircBotX(configuration);
		bot.startBot();
	}
}
