/*
    This program is a part of Botography which is an IRC bot.

    Copyright (C) 2016 Gerald Lam

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */
package botography;

import org.pircbotx.hooks.ListenerAdapter;
import org.pircbotx.hooks.events.ConnectEvent;
import org.pircbotx.hooks.events.MessageEvent;
import org.pircbotx.hooks.events.PrivateMessageEvent;

import botography.messages.CommandProcessor;

/**
 * Listens for commands, i.e. ".profile user."
 * 
 * @author Xerixe
 */
public class CommandListener extends ListenerAdapter {

	@Override
	public void onConnect(final ConnectEvent event) {
		event.respond("PRIVMSG NickServ :identify " + Settings.getLogin() + " "
				+ Settings.getPassword());
		event.respond("MODE " + Settings.getLogin() + " +B");
	}

	/**
	 * Channel chat
	 * 
	 * @param event
	 * @throws java.lang.Exception
	 */
	@Override
	public void onMessage(final MessageEvent event) throws Exception {
		CommandProcessor.processCommands(event, true);
	}

	/**
	 * Private Message
	 * 
	 * @param event
	 * @throws java.lang.Exception
	 */
	@Override
	public void onPrivateMessage(final PrivateMessageEvent event)
			throws Exception {
		CommandProcessor.processCommands(event, false);
	}

}
