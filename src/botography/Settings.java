/*
    This program is a part of Botography which is an IRC bot.

    Copyright (C) 2016 Gerald Lam

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package botography;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Xerixe
 */
public class Settings {
    
    private static final File CONFIG_FILE = new File("config.ini");
    
    private static final File USER_FILE = new File("user.ini");
    
    private static String server = "irc.snoonet.org:6667";
    private static String name = "Botography";
    private static String login = "Botography";
    private static String password = "test123";
    private static String channel = "##Botography";

    private static final Properties SETTINGS = new Properties();
    
    private static final HashMap<String, User> users = new HashMap<>();
    
    private static final ReentrantReadWriteLock userLock = new ReentrantReadWriteLock(true);
    
    public static void init() {
        try {
            if (!CONFIG_FILE.exists()) {
                if (!CONFIG_FILE.createNewFile()) {
                    Logger.getLogger(Settings.class.getName()).severe("Error creating settings file");
                    return;
                }
            }
            if (!USER_FILE.exists()) {
                if (!USER_FILE.createNewFile()) {
                    Logger.getLogger(Settings.class.getName()).severe("Error creating user file");
                    return;
                }
            }
            try (FileReader reader = new FileReader(CONFIG_FILE)) {
                SETTINGS.load(reader);
                server = get("server", server);
                name = get("name", name);
                login = get("login", login);
                password = get("password", password);
                channel = get("channel", channel);
                save(); //Incase default
            }
            try (BufferedReader br = new BufferedReader(new FileReader(USER_FILE))) {
                String line;
                userLock.writeLock().lock();
                try {
                    while ((line = br.readLine()) != null) {
                        if (!line.isEmpty()) {
                            String[] details = line.split(",");
                            User u = new User(details[0], details[1], details[2], details[3], details[4], details[5]);
                            users.put(details[0].toLowerCase(), u);
                        }
                    }
                } finally {
                    userLock.writeLock().unlock();
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(Settings.class.getName()).log(Level.SEVERE, "Error loading settings", ex);
        }
    }
    
    private static void save() {
        try (OutputStream out = new FileOutputStream(CONFIG_FILE)) {
            change("server", server);
            change("name", name);
            change("login", login);
            change("password", password);
            change("channel", channel);
            SETTINGS.store(out, null);
            out.flush();
        } catch (IOException ex) {
            Logger.getLogger(Settings.class.getName()).log(Level.SEVERE, "Error saving settings", ex);
        }

    }
    
    public static void saveUser() {
        userLock.writeLock().lock();
        try {
            try (BufferedWriter dayWriter = new BufferedWriter(new FileWriter(USER_FILE, false))) {
                for (Map.Entry<String, User> line : users.entrySet()) {
                    User u = line.getValue();
                    dayWriter.write(u.getUser() + ",");
                    dayWriter.write(u.getReddit() + ",");
                    dayWriter.write(u.getInstagram() + ",");
                    dayWriter.write(u.getFlickr() + ",");
                    dayWriter.write(u.get500px() + ",");
                    dayWriter.write(u.getWebsite());
                    dayWriter.write("\r\n");
                }
                dayWriter.flush();
            }
        } catch (IOException e) {
            Logger.getLogger(Settings.class.getName()).log(Level.SEVERE, "Error saving day settings", e);
        } finally {
            userLock.writeLock().unlock();
        }
    }
    
    public static void change(String name, String val) {
        SETTINGS.setProperty(name, val);
    }
    
    private static String get(String name, String def) {
        return SETTINGS.getProperty(name, def);
    }

    public static String getName() {
        return name;
    }

    public static String getLogin() {
        return login;
    }

    public static String getPassword() {
        return password;
    }

    public static String getChannel() {
        return channel;
    }

    public static String getServer() {
        return server.split(":")[0];
    }
    
    public static int getServerPort() {
        return Integer.parseInt(server.split(":")[1]);
    }
    
    public static User getUser(String name) {
        userLock.readLock().lock();
        try {
            return users.get(name.toLowerCase());
        } catch (NullPointerException ex) {
            return null;
        } 
        finally {
            userLock.readLock().unlock();
        }
    }
    
    public static User addIfNotExists(String name) {
        userLock.readLock().lock();
        if (users.containsKey(name.toLowerCase())) {
            userLock.readLock().unlock();
            return users.get(name.toLowerCase());
        } else {
            userLock.readLock().unlock();
            User u = new User(name);
            users.put(name.toLowerCase(), u);
            saveUser();
            return u;
        }
    }
    
}
