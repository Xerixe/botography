/*
    This program is a part of Botography which is an IRC bot.

    Copyright (C) 2016 Gerald Lam

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package botography;

/**
 *
 * @author Xerixe
 */
public class User {
    
    private String user;
    private String instagram;
    private String flickr;
    private String _500px;
    private String reddit;
    private String website;
    
    public User(String user) {
        this.user = user;
        this.instagram = " ";
        this.flickr = " ";
        this._500px = " ";
        this.reddit = " ";
        this.website = " ";
    }

    public User(String user, String reddit, String instagram, String flickr, String _500px, String website) {
        this.user = user;
        this.reddit = reddit;
        this.instagram = instagram;
        this.flickr = flickr;
        this._500px = _500px;
        this.website = website;
    }

    public String getUser() {
        return user;
    }

    public String getInstagram() {
        return instagram;
    }

    public String getFlickr() {
        return flickr;
    }

    public String get500px() {
        return _500px;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public void setFlickr(String flickr) {
        this.flickr = flickr;
    }

    public void set500px(String _500px) {
        this._500px = _500px;
    }

    public String getReddit() {
        return reddit;
    }

    public void setReddit(String reddit) {
        this.reddit = reddit;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }
    
    public boolean notEmpty() {
        return !reddit.equals(" ") || !flickr.equals(" ")
                || !instagram.equals(" ") || !_500px.equals(" ")
                || !website.equals(" ");
    }
    
    public boolean hasInstagram() {
        return !instagram.equals(" ");
    }
    
    public boolean hasReddit() {
        return !reddit.equals(" ");
    }
    
    public boolean hasFlickr() {
        return !flickr.equals(" ");
    }
    
    public boolean has500px() {
        return !_500px.equals(" ");
    }
    
    public boolean hasWebsite() {
        return !website.equals(" ");
    }
    
}
