/*
    This program is a part of Botography which is an IRC bot.

    Copyright (C) 2016 Gerald Lam

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */
package botography.messages;

import org.pircbotx.hooks.types.GenericMessageEvent;

/**
 * 
 * @author Xerixe
 */
public abstract class Command {

	public static final int LEVEL_UNREGISTERED = 0;
	public static final int LEVEL_NORMAL = 1;
	public static final int LEVEL_HALF_OP = 2;
	public static final int LEVEL_OP = 3;

	public static final String HELP_MSG = "Type .profile <username> in channel to display someones profile. "
			+ "/msg ProfileBot .ig, .flickr, .500px, .reddit or .website to link your profiles. "
			+ "Usernames for social media, the full URL for website (e.g. \"/msg ProfileBot .flickr toasti123\"). /msg ProfileBot .remove "
			+ "<type> to remove the specified link from your profile. If I'm doing something wrong, "
			+ "create an issue here: https://bitbucket.org/Xerixe/botography/issues";

	public static final String MISSING_SELF_PROFILE = "You have not linked your profile with me yet!";
	public static final String MISSING_PROFILE = "The user has not linked their profile with me yet!";
	public static final String INVALID_USERNAME = "Your username seems to be invalid, "
			+ "you only need to type in your username and not the full URL! How to set your flickr profile name: http://i.imgur.com/cxbMm8i.png";
	public static final String NO_SPACES = "You cannot have spaces in your username/website!";
	public static final String NO_COMMAS = "You cannot have commas in your website!";
	public static final String ERROR_REMOVE = "You need to type in .remove <type> to use this command!";
	public static final String PROFILE_TYPE = "The type needs to be either ig/flickr/reddit/500px/website";
	public static final String REGISTER = "You need to register and auth with snoonet services to use me. "
			+ "Type '/msg NickServ help register' or go to https://snoonet.org/anope#NickServ for more info.";
	public static final String UNKNOWN_COMMAND = "I'm sorry but I'm unable to decipher what you're asking me to do!"
			+ " Use \".help\" for a command list!";

	public static final String ADDED_PIGEON = "I stored your pigeon.";

	protected abstract CommandDefinition[] getDefinitions();

	protected abstract void execute(GenericMessageEvent event, String[] messages);

	protected void respond(final GenericMessageEvent event, final String message) {
		event.respond(message);
	}

}
