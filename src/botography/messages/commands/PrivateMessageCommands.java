/*
    This program is a part of Botography which is an IRC bot.

    Copyright (C) 2016 Gerald Lam

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package botography.messages.commands;

import botography.Settings;
import botography.User;
import botography.messages.Command;
import botography.messages.CommandDefinition;
import java.util.regex.Pattern;
import org.pircbotx.Channel;
import org.pircbotx.hooks.types.GenericMessageEvent;

/**
 *
 * @author Xerixe
 */
public class PrivateMessageCommands extends Command {
    
    @Override
    protected CommandDefinition[] getDefinitions() {
        return new CommandDefinition[] {
                new CommandDefinition("flickr", LEVEL_NORMAL, false),
                new CommandDefinition("ig", LEVEL_NORMAL, false),
                new CommandDefinition("instagram", LEVEL_NORMAL, false),
                new CommandDefinition("500px", LEVEL_NORMAL, false),
                new CommandDefinition("reddit", LEVEL_NORMAL, false),
                new CommandDefinition("website", LEVEL_NORMAL, false),
                new CommandDefinition("remove", LEVEL_NORMAL, false),
        };
    }
    
    @Override
    protected void execute(GenericMessageEvent event, String[] messages) {
        String name = event.getUserHostmask().getNick(); //Only returns correct registered name if level > normal
        switch (messages[0]) {
            case "remove": {
                User u;
                if (messages.length == 2) {
                    u = Settings.addIfNotExists(name);
                } else if (messages.length == 3) {
                    Channel channel = event.getBot().getUserChannelDao()
                            .getChannel(Settings.getChannel());
                    if (channel.isOp(event.getUser())) {
                        u = Settings.addIfNotExists(messages[2]);
                    } else {
                        respond(event, ERROR_REMOVE);
                        return;
                    }
                } else {
                    respond(event, ERROR_REMOVE);
                    return;
                }
                if (removeLink(messages[1], u.getUser())) {
                    respond(event, "I have updated " + (messages.length == 2 ? "your" : "the") + " profile.");
                } else {
                    respond(event, PROFILE_TYPE);
                }
                break;
            }
            case "flickr":
            case "ig":
            case "instagram":
            case "500px":
            case "reddit":
            case "website": {
                if (messages.length == 2) {
                    if (!messages[1].contains(",") || !messages[0].equals("website")) {
                        if (validUsername(messages[1]) || messages[0].equals("website")) {
                            User u = Settings.addIfNotExists(name);
                            switch (messages[0]) {
                                case "flickr":
                                    u.setFlickr(messages[1]);
                                    break;
                                case "ig":
                                case "instagram":
                                    u.setInstagram(messages[1]);
                                    break;
                                case "500px":
                                    u.set500px(messages[1]);
                                    break;
                                case "reddit":
                                    u.setReddit(messages[1]);
                                    break;
                                case "website":
                                    u.setWebsite(messages[1]);
                                    break;
                            }
                            event.respond("You have updated your " + getCapitalizeString(messages[0]) + " link.");

                            Settings.saveUser();
                        } else {
                            event.respond(INVALID_USERNAME);
                        }
                    } else {
                        respond(event, NO_COMMAS);
                    }
                } else {
                    respond(event, NO_SPACES);
                }
                break;
            }
        }
       
    }
    
    private static boolean removeLink(String type, String user) {
        User u = Settings.addIfNotExists(user);
        switch (type.toLowerCase()) {
            case "instagram":
            case "ig":
                u.setInstagram(" ");
                return true;
            case "reddit":
                u.setReddit(" ");
                return true;
            case "flickr":
                u.setFlickr(" ");
                return true;
            case "website":
                u.setWebsite(" ");
                return true;
            case "500px":
                u.set500px(" ");
                return true;
            default:
                return false;
        }
    }
    
    private static String getCapitalizeString(String type) {
        type = type.toLowerCase();
        if (type.equals("website")) {
            return type;
        } else if (type.equals("ig")) {
            type = "instagram";
        }
        return Character.toUpperCase(type.charAt(0)) + type.substring(1);
    }
    
    private static final Pattern REGEX = Pattern
            .compile("^[a-zA-Z0-9-_.]{2,20}$");

    private static boolean validUsername(final String name) {
        return REGEX.matcher(name).matches();
    }
}
