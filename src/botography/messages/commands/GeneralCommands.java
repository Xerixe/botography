/*
    This program is a part of Botography which is an IRC bot.

    Copyright (C) 2016 Gerald Lam

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */
package botography.messages.commands;

import java.util.concurrent.TimeUnit;

import org.pircbotx.hooks.events.MessageEvent;
import org.pircbotx.hooks.types.GenericMessageEvent;

import botography.PigeonManager;
import botography.Settings;
import botography.User;
import botography.messages.Command;
import botography.messages.CommandDefinition;
import botography.messages.CommandProcessor;

/**
 * 
 * @author Xerixe
 */
public class GeneralCommands extends Command {

	@Override
	protected CommandDefinition[] getDefinitions() {
		return new CommandDefinition[] {
				new CommandDefinition("profile", LEVEL_UNREGISTERED, true),
				new CommandDefinition("help", LEVEL_UNREGISTERED, true),
				new CommandDefinition("dunno", LEVEL_OP, true),
				new CommandDefinition("timeout", LEVEL_OP, true),
				new CommandDefinition("pigeon", LEVEL_UNREGISTERED, true) };
	}

	@Override
	@SuppressWarnings("StringConcatenationInsideStringBufferAppend")
	protected void execute(final GenericMessageEvent event,
			final String[] messages) {
		switch (messages[0]) {
			case "help": {
				printHelp(event);
				break;
			}
			case "dunno": {
				event.respondWith('\u00af' + "\\_(" + '\u30c4' + ")_/" + '\u00af');
				break;
			}
			case "pigeon": {
				if (messages.length < 3) {
					return;
				}
				String user = messages[1];
				String message = event.getMessage().split(" ", 3)[2] + " -"
						+ event.getUser().getNick();
				if (PigeonManager.newPigeon(user, message)) {
					event.respond(ADDED_PIGEON);
				}
				break;
			}
			case "timeout": {
				if (messages.length != 3) {
					return;
				}

				String user = messages[1];
				String channel;
				long time;

				try {
					channel = ((MessageEvent) event).getChannel().getName();
				} catch (java.lang.ClassCastException ex) {
					return;
				}
				try {
					time = Long.parseLong(messages[2]);
				} catch (NumberFormatException ex) {
					event.respond("not a valid time amount.");
					return;
				}

				if (!user.contains("!") && !user.contains("@")) {
					user += "!*@*";
				} else {
					String[] temp = user.split("!");
					if (temp.length != 2) {
						return;
					}
					temp = temp[1].split("@");
					if (temp.length != 2) {
						return;
					}
				}

				event.getBot().sendIRC().mode(channel, "+b " + user);
				event.respondWith("Timed out " + user + " for " + time
						+ " minutes. (Requested by "
						+ event.getUser().getNick() + ")");

				try {
					TimeUnit.MINUTES.sleep(time);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				event.getBot().sendIRC().mode(channel, "-b " + user);
				break;
			}
			case "profile": {
				String name;
				if (messages.length == 1) {
					name = CommandProcessor.getVerifiedName(event
							.getUserHostmask());
				} else {
					name = messages[1];
				}
				User u = Settings.getUser(name);
				if (u != null) {
					if (u.notEmpty()) {
						StringBuilder response = new StringBuilder("User: "
								+ u.getUser() + " ");
						if (u.hasReddit()) {
							response.append("Reddit: http://reddit.com/u/"
									+ u.getReddit() + " ");
						}
						if (u.hasInstagram()) {
							response.append("Instagram: http://instagram.com/"
									+ u.getInstagram() + " ");
						}
						if (u.hasFlickr()) {
							response.append("Flickr: http://flickr.com/photos/"
									+ u.getFlickr() + " ");
						}
						if (u.has500px()) {
							response.append("500px: http://500px.com/"
									+ u.get500px() + " ");
						}
						if (u.hasWebsite()) {
							response.append("Website: " + u.getWebsite() + " ");
						}
						event.respondWith(response.toString());
					} else {
						event.respond(MISSING_PROFILE);
					}
				} else if (messages.length == 1) {
					event.respond(MISSING_SELF_PROFILE);
				} else {
					event.respond(MISSING_PROFILE);
				}
			}
		}
	}

	private void printHelp(final GenericMessageEvent event) {
		event.respond(HELP_MSG);
	}

}
