/*
    This program is a part of Botography which is an IRC bot.

    Copyright (C) 2016 Gerald Lam

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package botography.messages;

import botography.Settings;
import botography.messages.commands.GeneralCommands;
import botography.messages.commands.PrivateMessageCommands;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import org.pircbotx.Channel;
import org.pircbotx.UserHostmask;
import org.pircbotx.hooks.Event;
import org.pircbotx.hooks.WaitForQueue;
import org.pircbotx.hooks.events.WhoisEvent;
import org.pircbotx.hooks.types.GenericMessageEvent;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Xerixe
 */
public class CommandProcessor {
    
    //LinkedHashMap for insertion order
    private static LinkedHashMap<String, Event> queue = new LinkedHashMap<>();
    
    //Lock needed as it's multithreaded.
    private static ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
    
    private static Map<String, CommandPair> commands = new HashMap<>();
    
    public static void init() {
        registerCommand(new PrivateMessageCommands());
        registerCommand(new GeneralCommands());
    }
    
    private static void registerCommand(Command command) {
        for (CommandDefinition def : command.getDefinitions()) {
            commands.put(def.command.toLowerCase(), new CommandPair(command, def));
        }
    }
    
    public static void processCommands(GenericMessageEvent event, boolean channel) {
        String message = event.getMessage();
        if (message.charAt(0) == '.' || message.charAt(0) == '!') {
            String[] splitted = message.split(" ");
            splitted[0] = splitted[0].substring(1);
            if (splitted[0].length() > 0) {
                splitted[0] = splitted[0].toLowerCase();
                if (commands.containsKey(splitted[0])) {
                    CommandPair pair = commands.get(splitted[0]);
                    CommandDefinition def = pair.getDefinition();
                    
                    //Does not execute private message commands
                    if (!def.channelChat && channel) {
                        return;
                    }
                    if (def.requiredLevel == Command.LEVEL_UNREGISTERED || def.requiredLevel <= getLevel(event)) {
                        try {
                            pair.command.execute(event, splitted);
                        } catch (Exception e) {
                            LoggerFactory.getLogger(CommandProcessor.class.getName())
                                    .error("Error Processing Command", e);
                        }
                    } else if (!channel && def.requiredLevel == Command.LEVEL_NORMAL) { //Only show error in private chat
                        event.respond(Command.REGISTER);
                    }
                } else if (!channel) { 
                    event.respond(Command.UNKNOWN_COMMAND);
                }
            }
        } else if (!channel) { 
            event.respond(Command.UNKNOWN_COMMAND);
        }
    }
    
    private static int getLevel(GenericMessageEvent event) {
        Channel channel = event.getBot().getUserChannelDao()
                .getChannel(Settings.getChannel());
        if (channel.isOp(event.getUser())) {
            return Command.LEVEL_OP;
        } else if (channel.isHalfOp(event.getUser())) {
            return Command.LEVEL_HALF_OP;
        } else if (getVerifiedName(event.getUserHostmask()) != null) {
            return Command.LEVEL_NORMAL;
        }
        return Command.LEVEL_UNREGISTERED;
    }
    
    public static String getVerifiedName(final UserHostmask hostmask) {
        /* Implementation based on org.pircbotx.User.isVerified() */
        try {
            hostmask.send().whoisDetail();
            WaitForQueue waitForQueue = new WaitForQueue(hostmask.getBot());
            while (true) {
                WhoisEvent event = waitForQueue.waitFor(WhoisEvent.class);
                if (!event.getNick().equals(hostmask.getNick())) {
                    continue;
                }
                waitForQueue.close();
                String registeredAs = event.getRegisteredAs();
                /*
                * An empty registeredAs string means we are registered on this
                * nick. Anything else means we're registered for a different
                * nick.
                 */
                if(registeredAs != null){
                    if (registeredAs.isEmpty()) {
                        return hostmask.getNick();
                    }
                }
                return registeredAs;
            }
        } catch (InterruptedException ex) {
            throw new RuntimeException("Couldn't finish querying user for verified status", ex);
        }
    }
    
    static class CommandPair {
        
        private Command command;
        private CommandDefinition definition;

        public CommandPair(Command command, CommandDefinition definition) {
            this.command = command;
            this.definition = definition;
        }

        public Command getCommand() {
            return command;
        }

        public CommandDefinition getDefinition() {
            return definition;
        }
        
    }
    
}
