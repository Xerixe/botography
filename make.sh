#!/bin/bash
find . -name "*.java" > javasources.txt
[ ! -d bin ] && mkdir bin
javac -classpath libs/pircbotx-2.1.jar:libs/commons-lang3-3.4.jar:libs/slf4j-simple-1.7.14.jar:libs/guava-19.0.jar:libs/slf4j-api-1.7.14.jar -d bin/ @javasources.txt
rm javasources.txt
